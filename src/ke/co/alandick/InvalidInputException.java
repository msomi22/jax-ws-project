/**
 * 
 */
package ke.co.alandick;

/**
 * @author peter
 *
 */
public class InvalidInputException extends Exception {
	
	private String errorDetails;
	
	public InvalidInputException(String reason, String errorDetails){
		super(reason);
		this.errorDetails = errorDetails;
	}
	
	public String getErrorInfo(){
		return errorDetails;
	}

}
