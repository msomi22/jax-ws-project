/**
 * 
 */
package ke.co.alandick;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

/**
 * @author peter
 *
 *
 *http://localhost:8081/jax-ws-project/ShopInfoService?wsdl
 */
@WebService
@SOAPBinding(style=Style.RPC)
public class ShopInfo {
	
	@WebMethod
	@WebResult(partName="lookupOutput")
	public String getShopInfo(@WebParam(partName="lookupInput") String property) throws InvalidInputException{
		String response = null;
		
		if("ShopName".equals(property)){
			response = "testmart";
			
		}else if("since".equals(property)){
			response = "since 2017";
			
		}else{
			throw new InvalidInputException("Invalid Input " , property);
		}
		return response;
	}

}
