/**
 * 
 */
package ke.co.alandick;

import java.util.List;

import javax.jws.WebService;

import ke.co.alandick.model.User;
import ke.co.alandick.users.UserServiceImpl;

/**
 * http://localhost:8081/jax-ws-project/SystemUsersService?wsdl
 * 
 * @author peter
 *
 */

@WebService(endpointInterface="ke.co.alandick.UserServiceDAO" , portName="SystemUsersPort", serviceName="SystemUsersService" )
public class UserService implements UserServiceDAO {
	
	UserServiceImpl userservice = new UserServiceImpl();

	
	
	/**
	 * @see ke.co.alandick.UserServiceDAO#getUsers()
	 */
	@Override
	public List<Object> getUsers(){
		return userservice.getUsers();
	}
	
	/**
	 * @see ke.co.alandick.UserServiceDAO#getAllUsers(java.lang.String)
	 */
	@Override
	public List<User> getAllUsers(String level){ 
		return userservice.getUsers(level);
	}
	
	/**
	 * @see ke.co.alandick.UserServiceDAO#addUser(java.lang.String, ke.co.alandick.model.User)
	 */
	@Override
	public boolean addUser(String level, User user){
		return userservice.addUser(level, user);
	}

}













