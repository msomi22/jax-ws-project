/**
 * 
 */
package ke.co.alandick.client;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

/**
 *  http://localhost:8081/jax-ws-project/SystemUsersService?wsdl
 *  
 * @author peter
 *
 */
public class ConsumeSoap {


    /**
     * Starting point for the SAAJ - SOAP Client Testing
     */
    public static void main(String args[]) {
        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();
            
            String level = "active";
            
            // Send SOAP Message to SOAP Server
            String url = Commons.getEndpoint();
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(level), url);

            // Process the SOAP Response
            //System.out.println("soapResponse: " + soapResponse);
            Commons.printSOAPResponse(soapResponse);

            soapConnection.close();
        } catch (Exception e) {
            System.err.println("Error occurred while sending SOAP Request to Server");
            System.out.println("Error occurred : (" + e.getMessage() + ")"); 
        }
    }
    
   
    
    

    /**
     * @param subscriberNo
     * @param registerFlag
     * @return
     * @throws Exception
     */
    private static SOAPMessage createSOAPRequest(String level) throws Exception {
    	
    	
    	/**
    	 * <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:alan="http://www.alandick.co.ke">
			   <soapenv:Header/>
			   <soapenv:Body>
			      <alan:getAllUsers>
			         <!--Optional:-->
			         <arg0>active</arg0>
			      </alan:getAllUsers>
			   </soapenv:Body>
			</soapenv:Envelope>
			
			
		   
		   <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:alan="http://www.alandick.co.ke">
		    <SOAP-ENV:Header/>
		     <SOAP-ENV:Body>
		      <alan:getAllUsers>
		       <arg0>0798568017</arg0>
		     </alan:getAllUsers>
		    </SOAP-ENV:Body>
		   </SOAP-ENV:Envelope>


			
			
			
    	 */
    	
        MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        
        String nameSpaceact = "alan";
        String nameSpaceactURI = "http://www.alandick.co.ke/";
        
        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(nameSpaceact, nameSpaceactURI );
        
        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement("getAllUsers",nameSpaceact);
        
       
        //elements
        SOAPElement levelElem = soapBodyElem.addChildElement("arg0");
        levelElem.addTextNode(level);
        
        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.setHeader("Content-Type", "application/soap+xml"); 
        headers.addHeader("SOAPAction",  nameSpaceactURI + "getAllUsers");
        
        
        /* Print the request message */
        System.out.print("Request SOAP Message = ");
        soapMessage.writeTo(System.out);
        System.out.println();

        return soapMessage;
    }

   
}






/**
 * 
 *  
 *  <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
 *     xmlns:alan="http://www.alandick.co.ke">
		   <soapenv:Header/>
		   <soapenv:Body>
		      <alan:addUser>
		         <!--Optional:-->
		         <arg0>active</arg0>
		         <!--Optional:-->
		         <arg1>
		            <!--Optional:-->
		            <firstname>Denno</firstname>
		            <!--Optional:-->
		            <lastname>Murithi</lastname>
		            <!--Optional:-->
		            <username>Kenya</username>
		            <!--Optional:-->
		            <UserEmail>kenya@gmail.com</UserEmail>
		            <!--Optional:-->
		            <password>12345</password>
		         </arg1>
		      </alan:addUser>
		   </soapenv:Body>
		</soapenv:Envelope>
		
		
		
		
		
		
		
		
		
		
 <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
 xmlns:alan="http://www.alandick.co.ke/">
   <soapenv:Header/>
   <soapenv:Body>
      <alan:getAllUsers>
         <!--Optional:-->
         <arg0>?</arg0>
      </alan:getAllUsers>
   </soapenv:Body>
</soapenv:Envelope>


 * 
 * 
 * 
 */















