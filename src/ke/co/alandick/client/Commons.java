package ke.co.alandick.client;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.xml.soap.SOAPMessage;


/**
 * Generate a random request Id from 0 to 10, 000
 * 
 * @author <a href="mailto:mwendapeter72@gmail.com">Peter mwenda</a>
 *
 */
public class Commons {

	/**
	 * 
	 */
	Commons(){}

	/**
	 * @return a random numeric as string 
	 */
	public static String getRequestId(){
		Random rand = new Random();
		int  n = rand.nextInt(10000) + 1;
		return String.valueOf(n); 
	}

	/**
	 * @return time stamp as string
	 */
	public static String getRequestTimeStamp(){ 
		return new Timestamp( new Date().getTime()).toString(); 
	}

	/**
	 * @return system version
	 */
	public static String getVersion(){
		return "v1";
	}

	/**
	 * @return
	 */
	public static String getPartnerCode(){
		return "1003";
	}

	/**
	 * @return
	 */
	public static String getPortalUserName(){
		return "ADC";
	}

	/**
	 * @return
	 */
	public static String getAPIUsername(){
		return "ADC";
	}

	/**
	 * @return
	 */
	public static String getAPIPassword(){
		return "b2d8b88e0fbdf312496484d658e72785";
	}

	
	/**
	 * @return
	 */
	public static String getEndpoint(){
		return "http://localhost:8081/jax-ws-project/SystemUsersService"; 
	}



	 /**
     * Method used to print the SOAP Response
     */
    public static void printSOAPResponse(SOAPMessage soapResponse) throws Exception {
    	
        String soapMessageString = soapMessageToString(soapResponse);
      
        List<Object> sdSubscriberList = null;
        if(SoapParser.soapStringMap(soapMessageString, "ns2:getAllUsersResponse" ,"1") != null){
        	sdSubscriberList = (List<Object>) SoapParser.soapStringMap(soapMessageString, "ns2:getAllUsersResponse", "1" );
        }
        
        for(Object response : sdSubscriberList){
        	System.out.println(response); 
        }
  
    }
    

    /**
     * @param message
     * @return
     */
    public static String soapMessageToString(SOAPMessage message) 
    {
        String result = null;

        if (message != null) 
        {
            ByteArrayOutputStream baos = null;
            try 
            {
                baos = new ByteArrayOutputStream();
                message.writeTo(baos); 
                result = baos.toString();
            } 
            catch (Exception e) 
            {
            } 
            finally 
            {
                if (baos != null) 
                {
                    try 
                    {
                        baos.close();
                    } 
                    catch (IOException ioe) 
                    {
                    }
                }
            }
        }
        return result;
    }   












}
