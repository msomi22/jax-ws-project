/**
 * 
 */
package ke.co.alandick.client;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author peter
 *
 */
public class SoapParser {

	/**
	 * 
	 */
	public SoapParser() {}

	public static List<Object> soapStringMap(String soapString, String rootNode, String decisionParam){ 

		List<Object> list = null;

		try {  
			
			DocumentBuilderFactory dbFactory 
			= DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = dbFactory.newDocumentBuilder();

			StringBuilder xmlStringBuilder = new StringBuilder();
			xmlStringBuilder.append(soapString);
			ByteArrayInputStream input =  new ByteArrayInputStream(
					xmlStringBuilder.toString().getBytes("UTF-8"));
			Document doc = builder.parse(input);
			// doc.getDocumentElement().normalize();

			list = new ArrayList<>(); 
			NodeList nList = doc.getElementsByTagName(rootNode);


			switch (decisionParam){

			case "1":
				
				for (int temp = 0; temp < nList.getLength(); temp++) {
					Node nNode = nList.item(temp);
					
					Element eElement = (Element) nNode;
					System.out.println("*********************************************************************");
					System.out.println("firstname: "+eElement .getElementsByTagName("firstname").item(0).getTextContent());
					System.out.println("lastname: "+eElement .getElementsByTagName("lastname").item(0).getTextContent());
					System.out.println("username: "+eElement .getElementsByTagName("username").item(0).getTextContent());
					System.out.println("UserEmail: "+eElement .getElementsByTagName("UserEmail").item(0).getTextContent());
					System.out.println("password: "+eElement .getElementsByTagName("password").item(0).getTextContent());
					//list.add(adSubscriber);
				}
				break;	

			
			default:
				break;

			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

}
