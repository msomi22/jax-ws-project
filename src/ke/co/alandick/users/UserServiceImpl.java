/**
 * 
 */
package ke.co.alandick.users;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ke.co.alandick.model.User;

/**
 * @author peter
 *
 */
public class UserServiceImpl {
	
	static List<User> activeUsers = new ArrayList<User>();
	static List<User> inactiveUsers = new ArrayList<User>();
	 
	
	public UserServiceImpl(){
		activeUsers.add(new User("msomi","Peter","Mwenda","peter@gmail.com","peter*#@"));
		activeUsers.add(new User("deno","Dennis","Mutegi","deno@gmail.com","deno123"));
		activeUsers.add(new User("alvo","Alvis","Langat","langat@gmail.com","langatalvo"));
		
		inactiveUsers.add(new User("makamu","Ferdinard","Makamu","makamu@gmail.com","makamu*&&^"));
		inactiveUsers.add(new User("charlo","Charles","Thurura","charo@gmail.com","12344"));
		inactiveUsers.add(new User("kmwenda","Kinoti","Mwenda","peter@gmail.com","adc245"));
		
	}
	
	public List<Object> getUsers(){
		List<Object> all_activeUsers = Stream.concat(activeUsers.stream(), inactiveUsers.stream()).collect(Collectors.toList());
		return all_activeUsers;
	}
	
	public List<User> getUsers(String level){
		switch(level.toLowerCase()){
		  case "active":
			  return activeUsers;
			  
		  case "inactive":
			  return inactiveUsers;
		}
		return null;
	}
	
	public boolean addUser(String level, User user){
		switch(level.toLowerCase()){
		  case "active":
			   activeUsers.add(user); 
			   break;
			  
		  case "inactive":
			  inactiveUsers.add(user); 
			  break;
			  
		 default:
			 return false;
		}
		
		return true;
	}

}
