/**
 * 
 */
package ke.co.alandick.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ke.co.alandick.Security;

/**
 * @author peter
 *
 */
@XmlRootElement(name="User")
@XmlType(propOrder={"firstname","lastname","username","email","password"})
public class User {
	
	private String username;
	private String firstname;
	private String lastname;
	private String email;
	private String password;
	
	//Any class annotated with XML annotation must have a null constructor 
	public User(){
		
	}
	/**
	 * @param username
	 * @param firstname
	 * @param lastname
	 * @param email
	 * @param password
	 */
	public User(String username, String firstname, String lastname,
			String email, String password) {
		super();
		this.username = username;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.password = password;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	@XmlElement(name="UserEmail")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return Security.getMD5(password);
		//Security.getMD5(password);
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", firstname=" + firstname
				+ ", lastname=" + lastname + ", email=" + email + ", password="
				+ password + "]";
	}
	
	

}
