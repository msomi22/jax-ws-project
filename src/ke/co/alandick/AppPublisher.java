/**
 * 
 */
package ke.co.alandick;

import javax.xml.ws.Endpoint;

/**
 * @author peter
 *
 */
public class AppPublisher {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		Endpoint.publish("http://localhost:8090/users", new UserService());
	}

}
