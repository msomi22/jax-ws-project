


package ke.co.alandick;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import ke.co.alandick.model.User;

/**
 * 
 * @author peter
 *
 */

@WebService(name="SystemUsers" ,  targetNamespace="http://www.alandick.co.ke/")
public interface UserServiceDAO {

	@WebMethod(action="allUsers", operationName="getAllUsers")
	@WebResult(name="act" , partName="act") 
	public abstract List<Object> getUsers();
	
	@WebMethod (action="activeUsers",operationName="getActiveUsers")
	@WebResult(name="level" , partName="get")
	public abstract List<User> getAllUsers(@WebParam(name = "level", partName = "level") String level);
	
	@WebMethod(action="putUser",operationName="putUser") 
	@WebResult(name="get" , partName="get")
	public abstract boolean addUser(String level, User user);

}